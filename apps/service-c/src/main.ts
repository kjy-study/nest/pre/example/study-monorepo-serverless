import { NestFactory } from '@nestjs/core';
import { ServiceCModule } from './service-c.module';

async function bootstrap() {
  const app = await NestFactory.create(ServiceCModule);
  await app.listen(3000);
}
bootstrap();
