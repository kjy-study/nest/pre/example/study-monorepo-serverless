import { Test, TestingModule } from '@nestjs/testing';
import { ServiceCController } from './service-c.controller';
import { ServiceCService } from './service-c.service';

describe('ServiceCController', () => {
  let serviceCController: ServiceCController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ServiceCController],
      providers: [ServiceCService],
    }).compile();

    serviceCController = app.get<ServiceCController>(ServiceCController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(serviceCController.getHello()).toBe('Hello World!');
    });
  });
});
